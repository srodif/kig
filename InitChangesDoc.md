# Kig KDE porting to Qt6 Notes

* Changed Qt version adn Kf version in root `CMakelists.txt` and Kf version at `kig/CMakeLists.txt`.
* Error module `KPluginLoader Class` missing at `kig/main.cpp`
  * Fixed by changing the module to `QPluginLoader Class`.
  * Also need to change where `Kpluginloader` was to `QPluginLoader`.
* Error module `KLocalizedString Class` missing at `kig/aboutdata.h` and also at `kig/kig.cpp`.
  * Fixed by adding (linking) in `kig/CMakeLists.txt` the relevant library `KF6::I18n` at `target_link_libraries`.
  * https://api.kde.org/frameworks/ki18n/html/index.html
* Error module `KTipDialog`  missing at `kig.kig.cpp`.
 * `KTipDialog` is deprecated. It also uses a `KTipDatabase` that is deprecated.
 * It is used `kig/kig.cpp` for enabling the Dialog of the day feature. The methods are included at `kig/kig.h`.
 * The proposed solution? Deprecate and remove the welcome tip of the day tool for this app because modern apps do not use it.
 * Maybe implement a tour of the app when loging in.
 * Links:
  * https://phabricator.kde.org/T11586
  * https://invent.kde.org/office/kmymoney/-/issues/22
  * https://kde.org/announcements/frameworks/5/5.83.0/
  * https://api.kde.org/legacy/pykde-4.5-api/kdeui/KTipDialog.html
* Error at using `I18N_NOOP`. These have been deprecated. https://www.volkerkrause.eu/2021/10/23/kf5-retiring-i18n-noop-macros.html
  * Proposed solution: https://invent.kde.org/frameworks/ki18n/-/merge_requests/21
  * In essence, changing the `I18N_NOOP` to `kli18n` or `ki18n`.
  * Maybe wrong: The `i18n()` function to `KLazyLocalizedString().toString()` or `KLocalizedString().toString`.
  * Remember to `#include <KLazyLocalizedString>`.
  * Note that ki18n returns a `KLazyLocalizedString` and not a `char` or `char []` or `char*` like `I18N_NOOP`. Check them.
  * This means that in `ArgsParser` objects, we need to change the specs so it does not expect a `std::string` but a `KLazyLocalizedString`.
  * Also, need to change the `specialActions` method from `object_type.h` and its descendants. Specifically, change from `QStringList` to `QList<KLazyLocalizedString>`.
  * Remember to also change Lists inside the method body declaration.
  * Also, change at `ObjectImpType` from `object_h` to accept updated parameters `KLazyLocalizedString`.
  * Also, redefinition of class `QStringList` happens at `coordinate_system.h`.
   * Commented out for now.
  * Consider the TRANSLATION_DOMAIN for the conversion! https://invent.kde.org/frameworks/ki18n/-/merge_requests/35    
  * So far found in:
   * `/objects/angle_type.cc`
   * `/objects/arc_type.cc`
   * `/objects/centerofcurvature_type.cc` and other objects.
* Error: main.cpp(94): error C3861: 'dataMigration': identifier not found
 * So far just commented out, will return.
* On `curve_imp.cc` , `qrand()` was not found.
 * Solution is to update deprecated `qrand()` and replace with `QRandomGenerator::global()->generate()`.
 * https://forum.qt.io/topic/115390/qt-5-15-depricated-qrand-is-deprecated-use-qrandomgenerator-instead
* On `objects/object_calcer.cc` `mem_fun` was not found as a member of `std`. Same for `bind2nd`. Also found on `objects/object_factory.cc`
 * One solution is to update deprecated `mem_fun` with improved `mem_fn`.
 * https://stackoverflow.com/questions/11680807/stdmem-fun-vs-stdmem-fn
 * https://en.cppreference.com/w/cpp/utility/functional/mem_fun
 * For `bind2nd` maybe it would better to replace with a lambda instead of `bind`, and so this is done.
 * https://stackoverflow.com/questions/32739018/a-replacement-for-stdbind2nd
*  
